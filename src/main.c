#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <sys/mman.h>

#define HEAP_SIZE 1000


static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)((uint8_t*)contents - offsetof(struct block_header, contents));
}

static void alloc_test(){
  heap_init(HEAP_SIZE);
  void *block = _malloc(REGION_MIN_SIZE);
  assert(block != NULL);
  _free(block);
  assert(block_get_header(block)->is_free);
  heap_term();
}

static void free_single_block_test(){
  heap_init(HEAP_SIZE);

  void *block1 = _malloc(REGION_MIN_SIZE);
  void *block2 = _malloc(REGION_MIN_SIZE);
  void *block3 = _malloc(REGION_MIN_SIZE);
  // test alloc
  assert(block1);
  assert(block2);
  assert(block3);

  _free(block2);

  assert(!block_get_header(block1)->is_free);
  assert(block_get_header(block2)->is_free);
  assert(!block_get_header(block3)->is_free);

  heap_term();
}

static void free_two_blocks_test(){
  heap_init(HEAP_SIZE);

  void *block1 = _malloc(REGION_MIN_SIZE);
  void *block2 = _malloc(REGION_MIN_SIZE);
  void *block3 = _malloc(REGION_MIN_SIZE);
  // test alloc
  assert(block1);
  assert(block2);
  assert(block3);

  _free(block1);
  _free(block2);

  assert(block_get_header(block1)->is_free);
  assert(block_get_header(block2)->is_free);
  assert(!block_get_header(block3)->is_free);

  heap_term();
}

static void new_reg_exp_old_test(){
  heap_init(HEAP_SIZE);

  void *block1 = _malloc(HEAP_SIZE);
  assert(block1);
  assert(!block_get_header(block1)->is_free);

  void *block2 = _malloc(REGION_MIN_SIZE);
  assert(block2);
  assert(!block_get_header(block2)->is_free);

  _free(block1);
  assert(block_get_header(block1)->is_free);

  _free(block2);
  assert(block_get_header(block2)->is_free);

  heap_term();
}

static void new_region_place_test(){
  void *heap = heap_init(HEAP_SIZE);
  void *mapped_address = mmap(heap, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
  assert(mapped_address!=MAP_FAILED);
  void *block = _malloc(REGION_MIN_SIZE);
  assert(block != NULL);
  _free(block);
  assert(block_get_header(block)->is_free);
  heap_term();
}
int main(){
  alloc_test();
  free_single_block_test();
  free_two_blocks_test();
  new_reg_exp_old_test();
  new_region_place_test();
  return 0;
}
